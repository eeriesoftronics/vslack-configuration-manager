# VSlack Configuration Manager

A tool to configure and backup configurations of Linux systems (should also work on other OS).

Main features

  * single configuration file to describe all the configuration backups you want
  * support for plugins if you need something more complex than simply file-copy or rsync between two folders

# Configuration File

The configuration file is where you specify which files/folders you want to copy and from where. It is also where you specify where to store the backup of these configuration files and folders.

## Locations

You can specify the location of the configuration file using `--config <path>`. This is not recommended though and `vs-cm` will look into the following locations for your configuration file (in this order):

  * `~/.vredenslack/cm/vs-cm.yml`
  * `/etc/vredenslack/cm/vs-cm.yml`
  * `./vs-cm.yml`

## Structure

The configuration file must follow the following structure

  base-folder: <path>
  configurations:
    <module name>:
      source: <path>
      type: <plugin name>
      ...

The base folder is where the backups will be stored as well as any other file or folder required by the configuration modules.

## Files Plugin

To activate the files plugin you need to provide a configuration module with the type `files` and a `list` setting where you specify the files and folders, relative to the `source`, which you want to backup.

  base-folder: <path>
  configurations:
    ...
    <module name>:
      source: <path>
      type: files
      list:
        - <file or folder>
        - <file or folder>
        - ...

## Rsync Plugin

This plugin uses the `rsync` tool, which must be available in your system. This plugin has some optional settings. The default ones are `-auqz`. It does not support all the rsync settings and currently it does not do remote connections, it simply rsyncs two folders in the same machine.

  base-folder: <path>
  configurations:
    ...
    <module name>:
      source: <path>
      type: rsync
      options:
        file: <rsync file>
        update: true|false
        delete: true|false

The `file` setting lets you define a rsync include filter file (used as `--include-from=<file>` argument to `rsync`). If you provide a relative path then it will be considered relative to the `base-folder`. A good standard is to create a `rsync` folder in the `base-folder` and put all your rsync files there.

The `update` and `delete` flags will enable/disable the same options for rsync. Note that the default value for `update` is `true` and for `delete` is `false`.

## Examples

This one is a sample of my user's vs-cm configuration file, which I place in `~/.vredenslack/cm/vs-cm.yml` so it gets automatically detected. It backups my Fluxbox, BASh environment and Pidgin settings, which I've split into three modules: `fluxbox`, `bash` and `pidgin`.

	base-folder: $HOME$/.vredenslack/cm
	configurations:
	  fluxbox:
		 source: $HOME$/.fluxbox
		 type: files
		 list:
			- apps
			- init
			- keys
			- menu
			- overlay
			- startup
			- windowmenu
			- styles/
	  bash:
		 source: $HOME$
		 files:
			- .bashrc
			- .bash_profile
			- .bash_ext
			- .bash_aliases
	  pidgin:
		 source: $HOME$/.purple/
		 type: rsync
		 options:
			file: rsync/pidgin.rsync
			delete: false

# Using it

After installation you should be able to run

  vs-cm
  
If you run it without any arguments it prints out a a quick reference text.

## Backup/Configure

To backup a single module:

	vs-cm backup <module name>

To backup all modules:

	vs-cm backup-all

Each time you backup a module it will overwrite the last backup you did.

To restore the last backup of a configuration module:

	vs-cm config <module name>

To restore the last backup of all configuration modules:

	vs-cm config-all

## Archive/Restore

Archiving will create a `.tar.gz` archive of the last backup of a configuration. This will create an archive with a timestamp which you can later restore. Use this to make more than one backup of your configurations.

	vs-cm archive <module name>
	vs-cm archive-all

Restore will not change the actual configurations of your system but will instead extract an archived configuration as the last backup.

	vs-cm restore <module name>

To make it easier, so you don't need to remember the actual timestamp of a backup you want, you can use

	vs-cm restore-list <module name>
	
to list all the archive timestamps for the given module.

## Export/Import

Export the base folder and store it as a .tar.gz file.

	vs-cm export <file>

Import an exported vs-cm folder. This will overwrite your current vs-cm folder so be careful.

	vs-cm import <file>
