#!perl -T
use 5.016;
use strict;
use warnings FATAL => 'all';
use Test::More;

BEGIN {
	use_ok( 'Vredenslack::Configuration::Manager' ) || print "Bail out!\n";
	use_ok( 'Vredenslack::Configuration::Base' ) || print "Bail out!\n";
	use_ok( 'Vredenslack::Configuration::Files' ) || print "Bail out!\n";
	
	my $d = {
		'base-folder' => './tst-1',
		'configurations' => {
			'test' => {
				'source' => './src-1',
				'type'   => 'files',
				'files'  => [
					'a.tst'
				]
			},
			'fail' => {
				'source' => './src-2', # source must not exist
				'type' => 'files',
				'files' => [
					'a.tst'
				]
			}
		}
	};
	ok(my $a = new Vredenslack::Configuration::Manager($d), 'new Manager');
	
}

done_testing();

diag( "Testing VSlack-CM $Vredenslack::Configuration::Manager::VERSION, Perl $], $^X" );
