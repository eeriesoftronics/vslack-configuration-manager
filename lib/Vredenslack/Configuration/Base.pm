package Vredenslack::Configuration::Base;

use warnings;
use strict;
use v5.12;

# debug
use Data::Dumper;

use Carp;
use Cwd qw/abs_path/;
use File::Path qw/make_path remove_tree/;

our $VERSION = '0.1';

sub new {
	my $class = shift;
	my $self = shift;
	
	croak 'invalid configuration' unless defined $self->{source} and defined $self->{type} and defined $self->{'base-folder'};
	
	# use the manager's parse_path function
	$self->{source} = Vredenslack::Configuration::Manager::_parse_location($self->{source});
	
	bless $self, $class;
}

sub _sync {
	my $self = shift;
	
	my $src = shift || croak 'no source provided';
	my $dst = shift || croak 'no destination provided';
	
	# make sure $src exists, croak otherwise
	croak $src, ' does not exist' unless -d $src;

	# create $dst or croak
	unless (-d $dst) {
		make_path $dst or croak 'could not create [', $dst, ']';
	}
	
	# call the underlying implementation
	$self->copy($src, $dst);
}

sub backup_to {
	my $self = shift;
	my $dst = shift;
	
	Vredenslack::Configuration::Manager::_log(3, 'backup from [', $self->{source}, '] to [', $dst, ']');
	$self->_sync($self->{source}, $dst);
}

sub config_from {
	my $self = shift;
	my $src = shift;
	
	Vredenslack::Configuration::Manager::_log(3, 'config from [', $src, '] to [', $self->{source}, ']');
	$self->_sync($src, $self->{source});
}

sub copy {
	my $self = shift;
	croak ref $self, ' does not implement the copy method';
}

1;

__END__

=head1 NAME

Vredenslack::Configuration::Base - base definition of a configuration descriptor

=head1 DESCRIPTION

This module is part of the L<Vredenslack::Configuration> tool.

=head1 PUBLIC METHODS

=head2 backup_to (folder)

Backup to a folder.

=head2 config_from (folder)

Configure the module from a backup folder.

=head1 VIRTUAL METHODS

These methods must be implemented by the classes extending this. The 
base class will implement these but as error catchers and thus will
cause a fatal error (croak).

=head2 copy (src, dst)

This methods should synchronize the configurations from src do dst.
These are two absolute paths for two folders.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
